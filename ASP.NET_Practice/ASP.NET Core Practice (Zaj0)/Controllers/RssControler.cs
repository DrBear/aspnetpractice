﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RSSGatherers.DataAccess.Getherers;
using RSSGatherers.DataAccess.Interfaces;

namespace ASP.NET_Core_Practice__Zaj0_.Controllers
{
    public class RssControler : Controller
    {
        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult RssList(string selecteditem, int columns)
        {
            var rssgatherersdict = new Dictionary<string, IGatherer>()
            {
                {"1",new GoogleNewsGatherer() },
                {"2", new BenchmarkArticlesGatherer()}
            };

            IGatherer rssGatherer = rssgatherersdict[selecteditem];

            return View(rssGatherer.GetAllSplitted(columns));
        }
    }
}