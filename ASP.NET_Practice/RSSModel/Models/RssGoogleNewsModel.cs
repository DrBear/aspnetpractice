﻿using System;

namespace RSSModel.Models
{
    public class RssGoogleNewsModel : IRssModel
    {
        public string Title { get; set; }
        public string Category { get; set; }
        public DateTime pubDate { get; set; }
        public string Description { get; set; }
    }
}