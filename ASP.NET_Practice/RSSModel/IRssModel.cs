﻿using System;

namespace RSSModel
{
    public interface IRssModel
    {
        string Title { get; set; }
        string Category { get; set; }
        DateTime pubDate { get; set; }
        string Description { get; set; }
    }
}