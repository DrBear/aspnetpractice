﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASP.NET_Practice__Zaj01_.Models;
using RSSGatherers.DataAccess.Getherers;
using RSSGatherers.DataAccess.Interfaces;

namespace ASP.NET_Practice__Zaj01_.Controllers
{
    public class RssController : Controller
    {

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult RssList(string selecteditem, int columns)
        {
            var rssgatherersdict = new Dictionary<string, IGatherer>()
            {
                {"1",new GoogleNewsGatherer() },
                {"2", new BenchmarkArticlesGatherer()}
            };

            IGatherer rssGatherer = rssgatherersdict[selecteditem];
       
            return View(rssGatherer.GetAllSplitted(columns));
        }
    }
}