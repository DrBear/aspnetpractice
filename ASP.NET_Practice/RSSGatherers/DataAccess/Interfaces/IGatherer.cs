﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using RSSModel;

namespace RSSGatherers.DataAccess.Interfaces
{
    public interface IGatherer
    {
        IEnumerable<IRssModel> RssList { get; set; }
        void Load();
        IEnumerable<IRssModel> GetAll();
        IEnumerable<IEnumerable<IRssModel>> GetAllSplitted(int columns);
    }
}