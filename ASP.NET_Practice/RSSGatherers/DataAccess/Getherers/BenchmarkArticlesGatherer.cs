﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using RSSGatherers.DataAccess.Interfaces;
using RSSGatherers.Tools;
using RSSModel;
using RSSModel.Models;

namespace RSSGatherers.DataAccess.Getherers
{
    public class BenchmarkArticlesGatherer : IGatherer
    {
        public BenchmarkArticlesGatherer()
        {
            Load();
        }
        public IEnumerable<IRssModel> RssList { get; set; }
        public void Load()
        {
            var rssdoc = XElement.Load("http://www.benchmark.pl/rss/testy-recenzje-minirecenzje.xml");
            RssList = rssdoc.Descendants("item").Select(x => new RssBenchmarkModel()
            {
                Title = x.Element("title")?.Value,
                Category = x.Element("category")?.Value,
                Description = x.Element("description")?.Value,
                pubDate = DateTime.Parse(x.Element("pubDate")?.Value)
            }).ToList();
        }

        public IEnumerable<IRssModel> GetAll()
        {
            return RssList;
        }

        public IEnumerable<IEnumerable<IRssModel>> GetAllSplitted(int columns)
        {
            return RssList.Split(columns);
        }
    }
}