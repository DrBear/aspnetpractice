﻿using System.Collections.Generic;
using System.Linq;

namespace RSSGatherers.Tools
{
    public static class Splitter
    {
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> rsslist, int columns)
        {
            for (int i = 0; i < (double)rsslist.Count()/columns; i++)
            {
                yield return rsslist.Skip(i * columns).Take(columns);
            }
        }
    }
}