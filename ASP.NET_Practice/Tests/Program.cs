﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSSGatherers.DataAccess.Getherers;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkArticlesGatherer rssGatherer = new BenchmarkArticlesGatherer();
            foreach (var item in rssGatherer.GetAll())
            {
                Console.WriteLine($"{item.Title},{item.Category},{item.pubDate.ToString()}");
            }

            Console.Read();
        }
    }
}
